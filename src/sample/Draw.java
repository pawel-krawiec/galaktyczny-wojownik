package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Draw {
  public  double x;
  public double y;
    private Image image;
    private double imageSize;
    private double imageRadius;

    public Draw(double x, double y, Image image, double imageSize, double imageRadius) {
        this.x = x;
        this.y = y;
        this.image = image;
        this.imageSize = imageSize;
        this.imageRadius = imageRadius;
    }

    public void draw(Canvas canvas, double x, double y) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        this.x = x - x % imageSize;
        this.y = y - y % imageSize;
        gc.drawImage(image, this.x, this.y, imageSize, imageSize);
    }






}
