package sample.png;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.GameObject;
import sample.Ship;

public class Warrior extends GameObject {
    public Warrior() {
        super( new ImageView(new Image(GameObject.class.getResourceAsStream("GWv1.png"), 40, 40, false, false)),true, new Point2D(0, 0),false) ;
    }

}