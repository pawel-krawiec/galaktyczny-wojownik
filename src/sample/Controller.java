package sample;

import javafx.animation.AnimationTimer;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import sample.png.Warrior;


public class Controller {
    @FXML
    public Pane pane;

    double[] pos = {300, 170};
    double[] vel = {0, 0};

    Ship ship = new Ship(false, 2, 0, pos, vel);

    public void onUpdate() {
        //todo
    }

    ;

    public Parent createContent() {

        Warrior warrior = new Warrior();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                onUpdate();
            }
        };
        pane.getChildren().add(warrior.getView());
        timer.start();

        return pane;
    }

    public void start() {
    }


    public void keyDown(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.NUMPAD8) {
            ship.setThrust(true);
            System.out.println("nacisniete");
        } else if (keyEvent.getCode() == KeyCode.NUMPAD4) {
            ship.setAngleVel(-0.05);
//            ship.update(canvas);
//            ship.draw(canvas);
//
        } else if (keyEvent.getCode() == KeyCode.NUMPAD6) {
            ship.setAngleVel(0.05);
//            ship.update(canvas);
//            ship.draw(canvas);
        } else if (keyEvent.getCode() == KeyCode.NUMPAD0) {
            ship.shoot();
        }
    }

    public void keyUp(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.NUMPAD8) {
            ship.setThrust(false);
        } else if (keyEvent.getCode() == KeyCode.NUMPAD4) {
            ship.setAngleVel(0);
        } else if (keyEvent.getCode() == KeyCode.NUMPAD6) {
            ship.setAngleVel(0);
        }

    }

    public void stop() {
        System.exit(0);
    }

    public void initialize() {
    }

}