package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.png.Warrior;

public class Ship {
    private boolean thrust;
    private double angle;
    private double angleVel;
    // private Image image;
    private ImageView imageView;
    private double imageSize;
    private double imageRadius;
    private double[] position;
    private double[] velocity;
    private final double DEFAULT_SIZE = 50;


    public Ship(boolean thrust, double angle, double angleVel, double[] position, double[] velocity) {
        this.thrust = thrust;
        this.angle = angle;
        this.angleVel = angleVel;
        //   this.image = new Image(Ship.class.getResourceAsStream("GWv1.png"));
        this.imageView = new ImageView(new Image(Warrior.class.getResourceAsStream("GWv1.png")));
        this.imageSize = DEFAULT_SIZE;
        this.imageRadius = imageRadius;
        this.position = position;
        this.velocity = velocity;
        imageView.setRotate(angle*180/Math.PI);
    }

    public boolean isThrust() {
        return thrust;
    }

    public void setThrust(boolean thrust) {
        this.thrust = thrust;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setAngleVel(double angleVel) {
        this.angleVel = angleVel;
    }

//    public void setImage(Image image) {
//        this.image = image;
//    }

    public void setImageSize(double imageSize) {
        this.imageSize = imageSize;
    }

    public void setImageRadius(double imageRadius) {
        this.imageRadius = imageRadius;
    }

    public void setPosition(double[] position) {
        this.position = position;
    }

    public void setVelocity(double[] velocity) {
        this.velocity = velocity;
    }

    public double getAngle() {
        return angle;
    }

    public double getAngleVel() {
        return angleVel;
    }

    public double[] getVelocity() {
        return velocity;
    }



    public void draw(Canvas canvas) {
        double ang = angle % (Math.PI/2);
        imageSize = DEFAULT_SIZE * Math.sin(ang) + DEFAULT_SIZE * Math.cos(ang);
        imageView.setRotate(angle *180 / Math.PI);




        // canvas.getGraphicsContext2D().drawImage(imageView.getImage(), position[0], position[1], imageSize, imageSize);
        canvas.getGraphicsContext2D().drawImage(imageView.snapshot(null, null), position[0]- imageSize/2, position[1] - imageSize/2, imageSize, imageSize);
//
    }

    public ImageView getImageView() {
        return imageView;
    }
    //    public Image getImage() {
//        return image;
//    }

    public void update(Canvas canvas) {
        this.angle += this.angleVel;
        angle %= 2* Math.PI;
        if (angle < 0){
            angle = 2* Math.PI - angle;
        }
        if (this.thrust) {
            for (int i = 0; i <= 1; i++) {
                this.velocity[i] += (0.1 * angleToVector(this.angle)[i]);
                ///jeżeli sound to dodaj funkcję dzwieku
            }
        } else {
            for (int i = 0; i <= 1; i++) {
                this.velocity[i] *= 0.99;

            }
        }
        for (int i = 0; i <= 1; i++) {
            this.position[i] += this.velocity[i];
        }
        this.position[0] = this.position[0] % canvas.getWidth();
        this.position[1] = this.position[1] % canvas.getHeight();

    }


    public double[] angleToVector(double x) {
        return new double[]{Math.cos(x), Math.sin(x)};
    }

    public void shoot() {
    }



}