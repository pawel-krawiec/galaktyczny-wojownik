package sample;

import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;

public abstract class GameObject {
    ImageView view;
    boolean alive;
    Point2D velocity;
    boolean thrust;


    public GameObject(ImageView view, boolean alive, Point2D velocity, boolean thrust) {
        this.view = view;
        this.alive = alive;
        this.velocity = velocity;
        this.thrust = thrust;
    }

    public boolean isAlive() {
        return alive;
    }
    public boolean isDead() {
        return !alive;
    }


    public ImageView getView() {
        return view;
    }

    public void setView(ImageView view) {
        this.view = view;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public Point2D getVelocity() {
        return velocity;
    }

    public void setVelocity(Point2D velocity) {
        this.velocity = velocity;
    }

    public boolean isThrust() {
        return thrust;
    }

    public void setThrust(boolean thrust) {
        this.thrust = thrust;
    }

    public void update() {
        view.setTranslateX(view.getTranslateX());
        view.setTranslateY(view.getTranslateY());
    }

    public boolean isColliding(GameObject other) {
        return getView().getBoundsInParent().intersects(other.getView().getBoundsInParent());
    }

}
